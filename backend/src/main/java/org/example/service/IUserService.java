package org.example.service;

import org.example.model.NoPswdUserView;
import org.example.model.User;

import java.util.List;

/**
 * User service interface.
 */

public interface IUserService {
    User getUserById(long id);
    NoPswdUserView getUserViewById(long id);
    List<NoPswdUserView> getAllUserViews();
    User addUser(User user);
    User updateUser(User user);
    Long deleteUser(Long id);
}
