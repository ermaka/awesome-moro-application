# Awesome Moro Application

A simple web application that allows to create, edit or delete users in a [Postgres](https://www.postgresql.org/) database.
Backend part of this application is implemented using [Spring](https://spring.io/) framework and its modules 
([Boot](https://spring.io/projects/spring-boot), [Web](https://www.baeldung.com/spring-mvc-tutorial), 
[Data JPA](https://spring.io/projects/spring-data-jpa), [Security](https://spring.io/projects/spring-security)).
GUI is made using [React.js](https://reactjs.org/) framework with [NPM](https://www.npmjs.com/) and 
[Webpack](https://webpack.js.org/) for compilation and assembly.

### Prerequisites

 - [Java 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
 - [Maven](https://maven.apache.org/)

### Build
```bash
mvn frontend:install-node-and-npm -pl frontend
mvn frontend:npm -pl frontend
mvn frontend:webpack -pl frontend
mvn clean install
```

### Profiles
There are two profiles exist, `dev` and `prod`. `dev` profile is used for development purposes.
It uses the embedded H2 database, which has a web console enabled and available at `http://localhost:8080/h2-console`.
You can log into this console using the credentials given below.

| parameter | description       | value                   |
|:---------:|:-----------------:|:-----------------------:|
| DB_URL    | database url      | jdbc:h2:mem:morosystems |
| DB_USER   | database username |            sa           |
| DB_PSWD   | database password |         password        |

`prod` profile uses external Postgres database and requires database credentials to be passed through environment variables.

| parameter | description       | example                           | required |
|:---------:|:-----------------:|:---------------------------------:|:--------:|
| DB_URL    | database url      | jdbc:postgresql://postgres/dbname |   true   |
| DB_USER   | database username |            username               |   true   |
| DB_PSWD   | database password |            password               |   true   |


### Run
To run the application, run the .jar package with environment variables. 
```bash
java -jar -Dspring.profiles.active=dev backend/target/awesome-moro-application.jar
# or
java -jar -Dspring.profiles.active=prod <database parameters with '-D' prefix> backend/target/awesome-moro-application.jar
```
Application is available at `http://localhost:8080`. 
It initializes the used database with test users, their credentials are listed below.

| name     | username | password  |
|:--------:|:--------:|:---------:|
| Alex     | alex     | pass_alex |
| Michaela | misa     | pass_misa |
| Tom      | tom      | pass_tom  |


### Docker
You can dockerize this application using the command `build-image` of 
[Spring Boot plugin](https://docs.spring.io/spring-boot/docs/current/maven-plugin/reference/htmlsingle/).
It builds a new [Docker](https://www.docker.com/) image named `awesome-moro-application:1.0`. 
```bash
mvn spring-boot:build-image -pl backend
```

Create and run a Docker container with the application, expose port 8080.
```bash
docker run awesome-moro-application:1.0 --p 8080:8080 ...
```

Use the attached `docker-compose.yml` to run application with a Postgres database with the `prod` profile.
```bash
docker-compose up -d
```

### Licence
[GPL v3](https://www.gnu.org/licenses/gpl-3.0)

### Author
[Aleksei Ermak](https://github.com/kazooo)