package org.example.repository;

import org.example.model.NoPswdUserView;
import org.example.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Simple CRUD repository for fetching user entities from database.
 * Here JpaRepository and PagingAndSortingRepository can also be used.
 */

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User getUserById(long id);
    NoPswdUserView getUserViewById(long id);
    List<NoPswdUserView> findAllProjectedBy();
    User getUserByUsername(String userName);
    Long deleteUserById(Long id);
}
