package org.example.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
@EqualsAndHashCode(callSuper = true)
public class ValidUserWithPswd extends ValidUser {
    @NotNull
    @Size(min = 8, message = "Password must be at least 8 characters long.")
    private String password;
}
