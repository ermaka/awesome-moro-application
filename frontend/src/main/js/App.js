const React = require('react');
const superagent = require('superagent');
import { Table, Header, Body, AddForm, UserRows } from './Table'

export const Context = React.createContext();

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {users: [], errorMessage: ''};
    }

    componentDidMount() {
        superagent.get('/api/users')
            .then(res => {
                this.setState({users: res.body, errorMessage: ''});
            }).catch(err => {
                this.setState({errorMessage: err.message});
            });
    }

    addUser = (user) => {
        superagent.post('/api/user')
            .send(user)
            .then(res => {
                let user = res.body;
                this.state.users.push(user);
                this.setState({users: this.state.users, errorMessage: ''});
            }).catch(err => {
                this.setState({errorMessage: err.response.body.error});
            });
    }

    updateUser = (user) => {
        superagent.put('/api/user')
            .send(user)
            .then(res => {
                let respUser = res.body;
                this.setState({
                    users: this.state.users.map((el) =>
                        el.id === respUser.id ? respUser : el
                    ), errorMessage: ''
                });
            }).catch(err => {
                this.setState({errorMessage: err.response.body.error});
            });
    }

    deleteUser = (id) => {
        superagent.delete('/secured/deleteUser/' + id)
            .then(res => {
                const filtered = this.state.users.filter((user) => user.id !== id);
                this.setState({users: filtered, errorMessage: ''});
            }).catch(err => {
                this.setState({errorMessage: err.response.body.error});
            });
    };

    render() {
        return (
            <Context.Provider
                value={{
                    addUser: this.addUser,
                    deleteUser: this.deleteUser,
                    updateUser: this.updateUser
                }}
            >
                <div className={"vertical"}>
                    <div className={"container"}>
                        <Table>
                            <Header values={['Id', 'Name', 'Username', 'Operations']}/>
                            <Body>
                                <UserRows users={this.state.users} />
                            </Body>
                        </Table>
                    </div>
                    <div className={"container"}>
                        <AddForm/>
                    </div>
                    { this.state.errorMessage && <ErrorMessage msg={this.state.errorMessage}/> }
                </div>
            </Context.Provider>
        )
    }
}

class ErrorMessage extends React.Component {
    render() {
        let errorMsgs;
        if (this.props.msg instanceof Array) {
            errorMsgs = this.props.msg.map((msg, i) => <span key={i}>{msg}<br/></span>);
        } else {
            errorMsgs = this.props.msg;
        }
        return <div className={"container"}>
            <h3 className="error container">{errorMsgs}</h3>
        </div>
    }
}

export default App;
