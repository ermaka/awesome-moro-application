package org.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * An entity structure that keeps the user's personal information.
 */

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull @Column(length = 25)
    @Size(min = 3, max = 25, message = "Name must be between 3 and 25 characters long.")
    private String name;
    @NotNull @Column(length = 25, unique = true)
    @Size(min = 3, max = 25, message = "Username must be between 3 and 25 characters long.")
    private String username;
    @NotNull @Column(length = 72)
    @Size(min = 8, message = "Password must be at least 8 characters long.")
    private String password;

    public User(ValidUserWithPswd validUserWithPswd) {
        name = validUserWithPswd.getName();
        username = validUserWithPswd.getUsername();
        password = validUserWithPswd.getPassword();
    }
}
