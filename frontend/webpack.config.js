const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/main/js/index.js',
    devtool: 'source-map',
    cache: true,
    mode: 'development',
    output: {
        path: __dirname,
        filename: './src/main/resources/static/built/bundle.js'
    },
    devServer: {
        contentBase: path.join(__dirname, './src/main/resources/static/built'),
        compress: true,
        port: 9000,
        proxy: {
            "/api/**": {
                target: 'http://localhost:8080'
            },
            "/secured/**": {
                target: "http://localhost:8080"
            }
        }
    },
    module: {
        rules: [
            {
                test: /.jsx$/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /.js$/,
                exclude: /(node_modules)/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ["@babel/preset-env", "@babel/preset-react"],
                        plugins: ['@babel/plugin-proposal-class-properties']
                    }
                }]
            }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
                template: "./src/main/resources/templates/index.html"
        })
    ]
};
