package org.example.service;

import lombok.AllArgsConstructor;
import org.example.model.NoPswdUserView;
import org.example.model.User;
import org.example.repository.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of user service interface that uses database repository.
 */

@Service
@AllArgsConstructor
@Transactional
public class DatabaseUserService implements IUserService {
   
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User getUserById(long id) {
        return userRepository.getUserById(id);
    }

    @Override
    public NoPswdUserView getUserViewById(long id) {
        return userRepository.getUserViewById(id);
    }

    @Override
    public List<NoPswdUserView> getAllUserViews() {
        return userRepository.findAllProjectedBy();
    }

    @Override
    public User addUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public User updateUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public Long deleteUser(Long id) {
        return userRepository.deleteUserById(id);
    }
}
