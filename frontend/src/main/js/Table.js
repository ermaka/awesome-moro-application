const React = require('react');
import PropTypes from "prop-types";
import {Context} from './App'
import '../css/styles.css'

class Table extends React.Component {
    render() {
        return <table className={"styled-table"}>{this.props.children}</table>
    }
}

class Header extends React.Component {
    render() {
        const heads = this.props.values.map((value, i) => <th key={i}>{value}</th>);
        return <thead><tr>{heads}</tr></thead>
    }
}

class Body extends React.Component {
    render() {
        return <tbody>{this.props.children}</tbody>
    }
}

class UserRows extends React.Component {
    render() {
        return this.props.users.map(user =>
            <Row key={user.id} user={user} />
        )
    }
}

class Row extends React.Component {

    constructor(props) {
        super(props);
        this.state = { inEdit: false };
    }

    editMode = () => {
        this.setState({ inEdit: true });
    }

    displayMode = () => {
        this.setState({ inEdit: false });
    }

    render() {
        if (this.state.inEdit) {
            return <EditForm user={this.props.user} closeEdit={this.displayMode} />
        } else {
            return <DisplayRow user={this.props.user} editMode={this.editMode} />
        }
    }
}

class DisplayRow extends React.Component {
    render() {
        return (
            <Context.Consumer>
                {context => (
                    <tr>
                        <Cell value={this.props.user.id} />
                        <Cell value={this.props.user.name} />
                        <Cell value={this.props.user.username} />
                        <td>
                            <Button label={'Delete'} handleSubmit={()=>context.deleteUser(this.props.user.id)} />
                            <Button label={'Edit'} handleSubmit={this.props.editMode} />
                        </td>
                    </tr>
                )}
            </Context.Consumer>
        );
    }
}

class AddForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {name: '', username: '', password: ''};
    }

    handleChange = (event) => {
        let state = this.state;
        state[event.target.name] = event.target.value
        this.setState(state);
    }

    handleClick = (parentFunc) => {
        parentFunc(this.state);
        this.setState({name: '', username: '', password: ''});
    }

    textFiled = (type, name, value, handleChange) => {
        return <td>
            <TextField type={type} name={name} value={value} handleChange={handleChange} />
        </td>
    }

    render() {
        return (
            <div>
                <Table>
                    <Header values={['Name', 'Username', 'Password']}/>
                    <Body>
                        <tr>
                            {this.textFiled('text', 'name', this.state.name, this.handleChange)}
                            {this.textFiled('text', 'username', this.state.username, this.handleChange)}
                            {this.textFiled('password', 'password', this.state.password, this.handleChange)}
                        </tr>
                    </Body>
                </Table>
                <div className={"block"}>
                    <Context.Consumer>
                        {context => (
                            <Button label={'Submit'} handleSubmit={() => this.handleClick(context.addUser)} />
                        )}
                    </Context.Consumer>
                </div>
            </div>
        )
    }
}

class EditForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: this.props.user.name,
            username: this.props.user.username
        };
    }

    handleChange = (event) => {
        let state = this.state;
        state[event.target.name] = event.target.value
        this.setState(state);
    }

    updateUser = (parentFunc) => {
        let { user } = this.props;
        user.name = this.state.name;
        user.username = this.state.username;
        parentFunc(user);
        this.props.closeEdit();
    }

    textFiled = (type, name, value, handleChange) => {
        return <td>
            <TextField type={type} name={name} value={value} handleChange={handleChange} />
        </td>
    }

    render() {
        return (
            <Context.Consumer>
                {context => (
                    <tr>
                        <td>{this.props.user.id}</td>
                        {this.textFiled('text', 'name', this.state.name, this.handleChange)}
                        {this.textFiled('text', 'username', this.state.username, this.handleChange)}
                        <td>
                            <Button label={"Submit"} handleSubmit={()=> this.updateUser(context.updateUser)} />
                            <Button label={"Cancel"} handleSubmit={()=> this.props.closeEdit()} />
                        </td>
                    </tr>
                )}
            </Context.Consumer>
        );
    }
}

class TextField extends React.Component {
    render() {
        return (
            <input type={this.props.type}
                   name={this.props.name}
                   value={this.props.value}
                   size="10"
                   onChange={this.props.handleChange}/>
        )
    }
}

class Cell extends React.Component {
    render() {
        return <td>{this.props.value}</td>
    }
}

class Button extends React.Component {
    render() {
        return <button type="submit" onClick={this.props.handleSubmit}>{this.props.label}</button>
    }
}

export {
    Table,
    Header,
    Body,
    AddForm,
    UserRows
}

Row.propTypes = {
    user: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
    }),
}

Cell.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
}

TextField.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    handleChange: PropTypes.func.isRequired
}

Button.propTypes = {
    label: PropTypes.string.isRequired,
    handleSubmit: PropTypes.func.isRequired
}
