package org.example.model;

import lombok.Data;
import org.example.validation.UniqueUsername;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
@UniqueUsername
public class ValidUser {
    private Long id;
    @NotNull
    @Size(min = 3, max = 25, message = "Name must be between 3 and 25 characters long.")
    private String name;
    @NotNull
    @Size(min = 3, max = 25, message = "Username must be between 3 and 25 characters long.")
    private String username;
}
