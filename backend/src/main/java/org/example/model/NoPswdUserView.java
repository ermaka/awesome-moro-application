package org.example.model;

public interface NoPswdUserView {
    Long getId();
    String getName();
    String getUsername();
}
