package org.example.validation;

import org.example.model.User;
import org.example.model.ValidUser;
import org.example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

/**
 * Validator that is used to ensure that the username is unique in a repository.
 * Handles cases where the user has changed all of the data but not the username.
 */

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, ValidUser> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean isValid(ValidUser user, ConstraintValidatorContext constraintValidatorContext) {
        Optional<User> dbUser = Optional.ofNullable(userRepository.getUserByUsername(user.getUsername()));
        return dbUser.map(u -> u.getId().equals(user.getId())).orElse(true);
    }
}
