package org.example.controller;

import lombok.AllArgsConstructor;
import org.example.service.IUserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Secured REST API controller for basic operations.
 */

@RestController
@RequestMapping("/secured")
@AllArgsConstructor
public class SecuredApiController {

    private final IUserService userService;

    @DeleteMapping("/deleteUser/{id}")
    ResponseEntity<?> updateUser(@PathVariable Long id) {
        Long deleted = userService.deleteUser(id);
        return deleted > 0 ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }
}
