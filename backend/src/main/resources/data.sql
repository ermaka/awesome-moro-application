insert into users (name, username, password) values
    ('Alex', 'alex', '$2a$10$xhhTyLxEbNTYsmplHM2AsOBNC37eAoaB.eU6292A9fJT1mzvZT.2u'),
    ('Michaela', 'misa', '$2a$10$m//11H2PKaLAFnxwULawaOV0ebOFeTmpSVQzQt0RxAIQgDblNO1ly'),
    ('Tom', 'tom', '$2a$10$cNJ7liXNTeESAk8n4vM7gOSdNltYp8LSQKOThYhkxVzkS8/Y1P0MS');
