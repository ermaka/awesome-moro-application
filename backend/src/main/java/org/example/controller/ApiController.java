package org.example.controller;

import lombok.AllArgsConstructor;
import org.example.model.NoPswdUserView;
import org.example.model.User;
import org.example.model.ValidUser;
import org.example.model.ValidUserWithPswd;
import org.example.service.IUserService;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * REST API controller for basic operations.
 */

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class ApiController {

    private final IUserService userService;

    @GetMapping("/user/{id}")
    public ResponseEntity<?> getUser(@PathVariable Long id) {
        Optional<NoPswdUserView> user = Optional.ofNullable(userService.getUserViewById(id));
        return user.map(resp -> ResponseEntity.ok().body(resp)).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/users")
    public List<NoPswdUserView> getUsers() {
        return userService.getAllUserViews();
    }

    @PostMapping("/user")
    ResponseEntity<?> createUser(@Valid @RequestBody ValidUserWithPswd newUserWithPswd,
                                 BindingResult bindingResult) throws URISyntaxException {
        if (bindingResult.hasErrors())
            return sendBadRequestResponse(bindingResult);
        User user = new User(newUserWithPswd);
        User result = userService.addUser(user);
        return ResponseEntity.created(new URI("/api/user/" + result.getId())).body(result);
    }

    @PutMapping("/user")
    ResponseEntity<?> updateUser(@Valid @RequestBody ValidUser validUser,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return sendBadRequestResponse(bindingResult);

        User user = userService.getUserById(validUser.getId());
        if (user == null)
            return ResponseEntity.notFound().build();

        user.setUsername(validUser.getUsername());
        user.setName(validUser.getName());
        User result = userService.updateUser(user);
        return ResponseEntity.ok().body(result);
    }

    private ResponseEntity<Map<String, Object>> sendBadRequestResponse(BindingResult bindingResult) {
        List<String> errorMsgs = getBindingErrorMsgs(bindingResult);
        Map<String, Object> error = new HashMap<>() {{
           put("error", errorMsgs);
        }};
        return ResponseEntity.badRequest().body(error);
    }

    private List<String> getBindingErrorMsgs(BindingResult bindingResult) {
        return bindingResult.getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
    }
}
